// Copyright 2024 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

// Package python enables integration testing against Python scripts.
package python // import "mellium.im/crypto/internal/python"

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"testing"
)

// Run creates a subtest that first starts a Python process and then executes
// some python and the given Go subtest, passing the stdin and stdout
// writer/reader for the test to use when communicating with the Python process.
func Run(t *testing.T, name, py string, f func(*testing.T, io.Writer, io.Reader)) {
	t.Run(name, func(t *testing.T) {
		// Create the python test and connect to its stdin/out/err.
		cmd := exec.Command("python", "-c", py)
		cmd.Dir = "testdata"
		stdin, err := cmd.StdinPipe()
		if err != nil {
			t.Fatalf("error connecting to python stdin: %v", err)
		}
		stderr, err := cmd.StderrPipe()
		if err != nil {
			t.Fatalf("error connecting to python stderr: %v", err)
		}
		// For stderr just buffer it and check if anything was output at the end so
		// that if we make a mistake in the python it's easier to debug.
		stderrBuf := bytes.NewBuffer(nil)
		go func() {
			_, err := stderrBuf.ReadFrom(stderr)
			if err != nil {
				panic(fmt.Errorf("error reading from stderr: %w", err))
			}
		}()
		t.Cleanup(func() {
			if stderrBuf.Len() > 0 {
				t.Fatalf("stderr:\n%s", stderrBuf.Bytes())
			}
		})
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			t.Fatalf("error connecting to python stdout: %v", err)
		}
		err = cmd.Start()
		if err != nil {
			t.Fatalf("error starting python command: %v", err)
		}

		f(t, stdin, stdout)
	})
}
