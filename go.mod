module mellium.im/crypto

go 1.21

require golang.org/x/crypto v0.18.0

require filippo.io/edwards25519 v1.1.0
