// Copyright 2024 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

// Package x3dh implements the X3DH key agreement protocol.
//
// X3DH was originally written for the Signal instant messaging client.
// For more information see the specification:
//
// https://signal.org/docs/specifications/x3dh/
package x3dh // import "mellium.im/crypto/x3dh"

import (
	"crypto/ecdh"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"hash"
	"io"

	"filippo.io/edwards25519"
	"golang.org/x/crypto/hkdf"
)

const (
	scalarSize = 32
)

// NewSignedPreKey creates a new X25519 signed prekey (SPK) and signs the public
// part of the key with the identity key, returning the private key and the
// signature.
func NewSignedPreKey(idKey ed25519.PrivateKey) (*ecdh.PrivateKey, []byte, error) {
	curve := ecdh.X25519()
	priv, err := curve.GenerateKey(rand.Reader)
	if err != nil {
		return nil, nil, err
	}
	pub := priv.PublicKey()
	sig := ed25519.Sign(idKey, pub.Bytes())

	return priv, sig, nil
}

func kdf(km []byte, h func() hash.Hash) ([]byte, error) {
	secret := make([]byte, scalarSize, scalarSize+len(km))
	for i := 0; i < len(secret); i++ {
		secret[i] = 0xff
	}
	secret = append(secret, km...)
	salt := make([]byte, h().Size())
	kdf := hkdf.New(h, secret, salt, []byte("mellium.im/crypto/x3dh"))

	dst := make([]byte, scalarSize)
	_, err := io.ReadFull(kdf, dst)
	return dst, err
}

// NewInitMessage creates a new initiation message that can be used to start an
// X3DH session.
// Returned values are the session key, any associated data, and the ephemeral
// public key.
func NewInitMessage(idKey ed25519.PrivateKey, peerIDKey, opkPub ed25519.PublicKey, spkPub *ecdh.PublicKey, spkSig []byte) ([]byte, []byte, *ecdh.PublicKey, error) {
	switch {
	case len(peerIDKey) != ed25519.PublicKeySize:
		err := fmt.Errorf("invalid public key size")
		return nil, nil, nil, err
	case !ed25519.Verify(peerIDKey, spkPub.Bytes(), spkSig):
		err := fmt.Errorf("invalid SPK signature")
		return nil, nil, nil, err
	}

	curve := ecdh.X25519()
	ekPriv, err := curve.GenerateKey(rand.Reader)
	if err != nil {
		return nil, nil, nil, err
	}

	idCrossKey, err := ED25519PrivToCurve25519(idKey)
	if err != nil {
		return nil, nil, nil, err
	}

	peerIDCrossKey, err := ED25519PubToCurve25519(peerIDKey)
	if err != nil {
		return nil, nil, nil, err
	}

	dh1, err := idCrossKey.ECDH(spkPub)
	if err != nil {
		return nil, nil, nil, err
	}
	dh2, err := ekPriv.ECDH(peerIDCrossKey)
	if err != nil {
		return nil, nil, nil, err
	}
	dh3, err := ekPriv.ECDH(spkPub)
	if err != nil {
		return nil, nil, nil, err
	}
	var dh4 []byte
	if len(opkPub) > 0 {
		opkPubCrossKey, err := ED25519PubToCurve25519(opkPub)
		if err != nil {
			return nil, nil, nil, err
		}
		dh4, err = ekPriv.ECDH(opkPubCrossKey)
		if err != nil {
			return nil, nil, nil, err
		}
	}

	dhOut := make([]byte, 0, 4*scalarSize)
	dhOut = append(dhOut, dh1...)
	dhOut = append(dhOut, dh2...)
	dhOut = append(dhOut, dh3...)
	dhOut = append(dhOut, dh4...)

	sessKey, err := kdf(dhOut, sha256.New)
	if err != nil {
		return nil, nil, nil, err
	}

	associatedData := append(idKey.Public().(ed25519.PublicKey), []byte(peerIDKey)...)
	return sessKey, associatedData, ekPriv.PublicKey(), nil
}

// RecvInitMessage handles an incoming X3DH request.
// Returned values are the session key and any associated data.
func RecvInitMessage(idKey, opkPriv ed25519.PrivateKey, spkPriv *ecdh.PrivateKey, peerIDKey ed25519.PublicKey, ekPub *ecdh.PublicKey) ([]byte, []byte, error) {
	idCrossKey, err := ED25519PrivToCurve25519(idKey)
	if err != nil {
		return nil, nil, err
	}

	peerIDCrossKey, err := ED25519PubToCurve25519(peerIDKey)
	if err != nil {
		return nil, nil, err
	}

	dh1, err := spkPriv.ECDH(peerIDCrossKey)
	if err != nil {
		return nil, nil, err
	}
	dh2, err := idCrossKey.ECDH(ekPub)
	if err != nil {
		return nil, nil, err
	}
	dh3, err := spkPriv.ECDH(ekPub)
	if err != nil {
		return nil, nil, err
	}
	var dh4 []byte
	if len(opkPriv) > 0 {
		opkPrivCrossKey, err := ED25519PrivToCurve25519(opkPriv)
		if err != nil {
			return nil, nil, err
		}
		dh4, err = opkPrivCrossKey.ECDH(ekPub)
		if err != nil {
			return nil, nil, err
		}
	}

	dhOut := make([]byte, 0, 4*scalarSize)
	dhOut = append(dhOut, dh1...)
	dhOut = append(dhOut, dh2...)
	dhOut = append(dhOut, dh3...)
	dhOut = append(dhOut, dh4...)

	sessKey, err := kdf(dhOut, sha256.New)
	if err != nil {
		return nil, nil, err
	}

	associatedData := append([]byte(peerIDKey), idKey.Public().(ed25519.PublicKey)...)
	return sessKey, associatedData, err
}

// ED25519PrivToCurve25519 converts an ED25519 secret key to an ECDH key on the
// X25519 curve.
func ED25519PrivToCurve25519(sk ed25519.PrivateKey) (*ecdh.PrivateKey, error) {
	h := sha512.New()
	_, _ = h.Write(sk.Seed())
	out := h.Sum(nil)
	out[0] &= 248
	out[31] &= 127
	out[31] |= 64
	return ecdh.X25519().NewPrivateKey(out[:ed25519.SeedSize])
}

// ED25519PubToCurve25519 converts an ED25519 public key to an ECDH key on the
// X25519 curve.
func ED25519PubToCurve25519(key ed25519.PublicKey) (*ecdh.PublicKey, error) {
	peerIDCrossPoint, err := new(edwards25519.Point).SetBytes([]byte(key))
	if err != nil {
		return nil, err
	}
	peerIDCrossKey := peerIDCrossPoint.BytesMontgomery()
	return ecdh.X25519().NewPublicKey(peerIDCrossKey)
}
