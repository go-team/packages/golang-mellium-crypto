// Copyright 2024 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package x3dh_test

import (
	"crypto/ed25519"
	"encoding/hex"
	"testing"

	"mellium.im/crypto/x3dh"
)

func TestED25519PubToCurve25519(t *testing.T) {
	// These known inputs and outputs were generated with libsodium.
	const (
		ed25519Key    = "5b53e339a1d4b77812e2e848dc9a25074bdb5351458afb838e30a3c0924e997b"
		curve25519Key = "7b8857475f907d8aeb66dc8b69aa216d7d07f29cadf14a69b8a41333e560b704"
	)
	pubBytes, err := hex.DecodeString(ed25519Key)
	if err != nil {
		t.Fatalf("invalid input: %v", err)
	}
	ed25519PubKey := ed25519.PublicKey(pubBytes)

	ecdhKey, err := x3dh.ED25519PubToCurve25519(ed25519PubKey)
	if err != nil {
		t.Fatalf("error converting key: %v", err)
	}
	if s := hex.EncodeToString(ecdhKey.Bytes()); s != curve25519Key {
		t.Fatalf("invalid output: want=%s,\n got=%s", curve25519Key, s)
	}
}

func TestED25519PrivToCurve25519(t *testing.T) {
	// These known inputs and outputs were generated with libsodium.
	const (
		ed25519Key    = "c228babb77356a24ad304b54cb7f9548e3ec2cf421d9364e4ad5971936dad718"
		curve25519Key = "a079c5f532409802406da10d9ab5a69625d3c57824bdb8af74ddfdcde72da44a"
	)
	pubBytes, err := hex.DecodeString(ed25519Key)
	if err != nil {
		t.Fatalf("invalid input: %v", err)
	}
	ed25519PrivKey := ed25519.NewKeyFromSeed(pubBytes)

	ecdhKey, err := x3dh.ED25519PrivToCurve25519(ed25519PrivKey)
	if err != nil {
		t.Fatalf("error converting key: %v", err)
	}
	if s := hex.EncodeToString(ecdhKey.Bytes()); s != curve25519Key {
		t.Fatalf("invalid output:\nwant=%s,\n got=%s", curve25519Key, s)
	}
}
