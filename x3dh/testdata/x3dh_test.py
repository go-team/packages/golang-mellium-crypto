#!/usr/bin/env python

from typing import Dict, Type, Any

import asyncio
import json
import sys
import x3dh

bundles: Dict[bytes, x3dh.Bundle] = {}


class IntegrationTestState(x3dh.State):
    def _publish_bundle(self, bundle: x3dh.Bundle) -> None:
        bundles[bundle.identity_key] = bundle

    @staticmethod
    def _encode_public_key(key_format: x3dh.IdentityKeyFormat, pub: bytes) -> bytes:
        return pub


def test_negotiation_recv() -> None:
    """
    This test performs a negotiation over STDIN and STDOUT and assumes the other
    side goes first.

    Args:
        None

    Returns:
        None

    Raises:
        None
    """

    # Begin by creating a set of keys and transfering the information to the Go
    # side of things (by printing to stdout) so that it can create the initial
    # message.
    state_settings: Dict[str, Any] = {
        "identity_key_format": x3dh.IdentityKeyFormat.ED_25519,
        "hash_function": x3dh.HashFunction.SHA_256,
        "info": b'mellium.im/crypto/x3dh',
    }
    state_recv = IntegrationTestState.create(**state_settings)
    print(json.dumps(state_recv.json))

    # Now receive the header, associated data, and (if applicable) pre-key sent
    # by Go:
    data = json.loads(sys.stdin.readline())

    if state_recv.bundle.identity_key in bundles:
        bundle_recv = bundles[state_recv.bundle.identity_key]
    else:
        assert False
    header = x3dh.Header(bytes.fromhex(data["identity_key_pub"]),
                         bytes.fromhex(data["ekey"]),
                         bundle_recv.signed_pre_key, None)

    # Run the exchange as the initiating and receiving party and send back the
    # final shared secret(s) to Go.
    (shared_secret_recv, associated_data_recv, _) = asyncio.run(state_recv.get_shared_secret_passive(
        header,
        associated_data_appendix=b'',
        require_pre_key=False))
    print(json.dumps({
        "shared_secret": shared_secret_recv.hex(),
        "associated_data": associated_data_recv.hex(),
    }))


def test_negotiation_init() -> None:
    """
    This test performs a negotiation over STDIN and STDOUT with itself
    initiating the key exchange.

    Args:
        None

    Returns:
        None

    Raises:
        None
    """

    # Begin by receiving the bundle from Go:
    data = json.loads(sys.stdin.readline())
    bundle = x3dh.types.Bundle(
        identity_key=bytes.fromhex(data["identity_key_pub"]),
        signed_pre_key=bytes.fromhex(data["spk_pub"]),
        signed_pre_key_sig=bytes.fromhex(data["spk_sig"]),
        pre_keys=[])

    # Now transmit the initial key exchange message
    state_settings: Dict[str, Any] = {
        "identity_key_format": x3dh.IdentityKeyFormat.ED_25519,
        "hash_function": x3dh.HashFunction.SHA_256,
        "info": b'mellium.im/crypto/x3dh',
    }
    state = IntegrationTestState.create(**state_settings)
    (shared_secret, associated_data, header) = asyncio.run(state.get_shared_secret_active(
        bundle,
        associated_data_appendix=b'',
        require_pre_key=False))
    print(json.dumps({
        "shared_secret": shared_secret.hex(),
        "associated_data": associated_data.hex(),
        "idkey": header.identity_key.hex(),
        "ek": header.ephemeral_key.hex(),
        "spk": header.signed_pre_key.hex(),
    }))
