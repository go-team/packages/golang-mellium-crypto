# crypto

[![Issue Tracker][badge]](https://mellium.im/issue)
[![Docs](https://pkg.go.dev/badge/mellium.im/crypto)](https://pkg.go.dev/mellium.im/crypto)
[![Chat](https://img.shields.io/badge/XMPP-users@mellium.chat-orange.svg)](https://mellium.chat)
[![License](https://img.shields.io/badge/license-FreeBSD-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

<a href="https://opencollective.com/mellium" alt="Donate on Open Collective"><img src="https://opencollective.com/mellium/donate/button@2x.png?color=blue" width="200"/></a>


This library implements various cryptographic protocols used by other Mellium
libraries.
It differs from [`mellium.im/xmpp/crypto`][xmpp/crypto]
It was written to be used as part of the OMEMO e2e encryption protocol.

To use it in your project, import it (or any of its other packages) like so:

```go
import mellium.im/crypto
```

If you'd like to contribute to the project, see [CONTRIBUTING.md].


## License

The package may be used under the terms of the BSD 2-Clause License a copy of
which may be found in the file "[LICENSE]".

Unless you explicitly state otherwise, any contribution submitted for inclusion
in the work by you shall be licensed as above, without any additional terms or
conditions.


[badge]: https://img.shields.io/badge/style-mellium%2fxmpp-green.svg?longCache=true&style=popout-square&label=issues
[docs]: https://pkg.go.dev/mellium.im/crypto
[CONTRIBUTING.md]: https://mellium.im/docs/CONTRIBUTING
[LICENSE]: https://codeberg.org/mellium/xmpp/src/branch/main/LICENSE
[xmpp/crypto]: https://pkg.go.dev/mellium.im/xmpp/crypto

